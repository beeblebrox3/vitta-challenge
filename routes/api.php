<?php

Route::resource('territories', 'TerritoryController');
Route::patch('territories/{id}/paint', 'TerritoryController@paint');
Route::delete('territories/{id}/paint', 'TerritoryController@removePaint');

//Route::resource('squares', 'SquareController');
Route::get('squares/{x}/{y}', 'SquareController@show');
Route::patch('squares/{x}/{y}', 'SquareController@update');

Route::get("foo", function () {
    $painter = new \App\Helpers\HtmlMapDrawer(10, 10);
    $painter->paintArea(0, 0, 2, 2, "red");

    $painter->paintPoint(6, 6, "blue");
    $painter->paintPoint(6, 8, "blue");

    $painter->paintPoint(8, 6, "blue");
    $painter->paintPoint(9, 7, "blue");
    $painter->paintPoint(8, 8, "blue");
    echo $painter->draw();
});
