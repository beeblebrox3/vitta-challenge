<?php

use Illuminate\Support\Facades\Schema;
use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerritoriesCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('territories'); // @todo dropIfExists doesnt work with mongodb?
        Schema::create('territories', function (Blueprint $collection) {
            $collection->index(['location' => '2dsphere'], null, null, ['min' => 0, 'max' => 112]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('territories');
    }
}
