<!doctype html>

<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dashboard | @beeblebrox3/vitta-challenge</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
</head>
<body>
<div class="flex-center position-ref full-height">

    <article>
        <h1>Dashboard</h1>

        <section>
            <h2>Territories by most painted areas</h2>

            <table>
                <thead>
                    <tr>
                        <th>Territory</th>
                        <th>Painted Area</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($territories->sortByDesc('painted_area') as $territory)
                    <tr>
                        <td>{{ $territory->name }}</td>
                        <td>{{ $territory->painted_area }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </section>

        <section>
            <h2>Territories by most porportional painted areas</h2>

            <table>
                <thead>
                <tr>
                    <th>Territory</th>
                    <th>Painted Area</th>
                </tr>
                </thead>
                <tbody>
                @foreach($territories->sortByDesc('proportional_painted_area') as $territory)
                    <tr>
                        <td>{{ $territory->name }}</td>
                        <td>{{ $territory->proportional_painted_area }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </section>

        <section>
            <h2>Last 5 painted squares</h2>
            <table>
                <thead>
                <tr>
                    <th>Square</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach($squares as $square)
                    <tr>
                        <td>[{{ $square->x }}, {{ $square->y }}]</td>
                        <td>{{ $square->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </section>

        <section>
            <h2>Last 5 errors</h2>

            <table>
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>File</th>
                        <th>Line</th>
                        <th>Message</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($logs as $log)
                    <tr>
                        <td>{{ $log->type }}</td>
                        <td>{{ $log->file }}</td>
                        <td>{{ $log->line }}</td>
                        <td>{{ $log->message }}</td>
                        <td>{{ $log->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </section>

        <section>
            <h2>Painted area / total area</h2>

            <table>
                <thead>
                    <tr>
                        <th>Total Area</th>
                        <th>Total Painted Area</th>
                        <th>Percentage</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $totalArea }}</td>
                        <td>{{ $totalPaintedArea }}</td>
                        <td>{{ round($totalPaintedArea / max(1, $totalArea) * 100, 2) }}</td>
                    </tr>
                </tbody>
            </table>
        </section>

        <section>
            <h2>Chart</h2>

            {!! $chart !!}
        </section>
    </article>
</div>
</body>
</html>
