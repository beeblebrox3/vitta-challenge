# Vitta Challenge

[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/beeblebrox3/vitta-challenge/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/beeblebrox3/vitta-challenge/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/g/beeblebrox3/vitta-challenge/badges/coverage.png?b=master)](https://scrutinizer-ci.com/g/beeblebrox3/vitta-challenge/?branch=master)
[![Build Status](https://scrutinizer-ci.com/g/beeblebrox3/vitta-challenge/badges/build.png?b=master)](https://scrutinizer-ci.com/g/beeblebrox3/vitta-challenge/build-status/master)

## Requirements
- Docker
- Docker Compose

### Execution
Clone this repository, create a `.env` file and create the containers:

```
git clone https://github.com/beeblebrox3/vitta-challenge
cd vitta-challenge
cp .env.example .env
docker-compose up -d
```

#### API
Basepath: http://localhost:8000/api/

| Method  | URI                                | Description                         |
| ------- | ---------------------------------- | ------------------------------------|
| GET     | /api/territories                   | List territories                    |
| GET     | /api/territories/{id}              | Show a territory                    |
| POST    | /api/territories                   | Create a new territory              |
| DELETE  | /api/territories/{id}/paint        | Delete a territory                  |
| PATCH   | /api/territories/{id}/paint        | Paint all squares from the territory|
| PATCH   | /api/squares/{x}/{y}               | Paint a square                      |
| GET     | /api/squares/{x}/{y}               | Show a square                       |


#### Dashboard
On the browser, go to [http://localhost:8000/dashboard](http://localhost:8000/dashboard)

### Tests
```
docker exec vitta-challenge-app composer test
```

If you want coverage report, just run:
```
docker exec vitta-challenge-app composer test-coverage
```