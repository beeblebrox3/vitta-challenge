<?php

namespace Tests\Feature;

use Tests\Helpers\ManageTerritories;
use Tests\TestCase;

class SquaresTest extends TestCase
{
    use ManageTerritories;

    public function setUp()
    {
        parent::setUp();
        $this->artisan("migrate:reset");
        $this->artisan("migrate");
    }

    public function testShouldGetSquare()
    {
        $this->json("GET", "/api/squares/1/2")
            ->assertStatus(404)
            ->assertJson(['error' => 'squares/not-found']);

        $territory = [
            'name' => 'A',
            'start' => ['x' => 0, 'y' => 0],
            'end' => ['x' => 10, 'y' => 10],
        ];
        $territoryInstance = $this->insertTerritory($territory);

        $this->json("GET", "/api/squares/1/2")
            ->assertStatus(200)
            ->assertJson([
                'data' => ['x' => 1, 'y' => 2, 'painted' => false]
            ]);

        $this->json("PATCH", "/api/squares/1/2", ['color' => 'blue'])
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'x' => 1,
                    'y' => 2,
                    'painted' => true,
                    'color' => 'blue'
                ]
            ]);

        $this->json("PATCH", "/api/squares/2/2", ['color' => 'red'])
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'x' => 2,
                    'y' => 2,
                    'painted' => true,
                    'color' => 'red'
                ]
            ]);

        $res = $this->json("GET", "/api/territories/{$territoryInstance->_id}?withpainted=true")
            ->assertStatus(200)
            ->assertJson([
                'data' => array_merge(
                    $territory,
                    ['painted_area' => 2, 'painted_squares' => [
                        ['x' => 1, 'y' => 2, 'color' => 'blue'],
                        ['x' => 2, 'y' => 2, 'color' => 'red'],
                    ]]
                ),
                'error' => false
            ]);

    }
}