<?php

namespace Tests\Feature;

use Tests\Helpers\ManageTerritories;
use Tests\TestCase;

class TerritoriesTest extends TestCase
{
    use ManageTerritories;

    public function setUp()
    {
        parent::setUp();
        $this->artisan("migrate:reset");
        $this->artisan("migrate");
    }

    public function testAddTerritory()
    {
        $input = [
            "name" => "A",
            "start" => ["x" => 0, "y" => 0],
            "end" => ["x" => 50, "y" => 50],
        ];

        $expected = [
            "data" => [
                "name" => "A",
                "start" => ["x" => 0, "y" => 0],
                "end" => ["x" => 50, "y" => 50],
                "area" => 2601, // (51 * 51),
                "location" => [
                    "type" => "Polygon",
                    "coordinates" => [[
                        [0, 0], [0, 50], [50, 50], [50, 0], [0, 0]
                    ]]
                ]
            ],
            "error" => false
        ];

        $res = $this->json("POST", "/api/territories", $input);
        $res->assertStatus(200)
            ->assertJson($expected);

        // Detect territory overlapping
        $res = $this->json("POST", "/api/territories", $input);
        $res->assertStatus(400)
            ->assertJson([
                "data" => $input,
                "error" => "territories/territory-overlay"
            ]);

        // Don't create if data is incomplete
        $inputs = [
            ['name' => "foo"],
            ['name' => "foo", "end" => ["x" => 1, "y" => 1]],
            ['name' => "foo", "start" => ["y" => 1], "end" => ["x" => 1, "y" => 1]],
            ['name' => "foo", "start" => ["x" => 1, "y" => 1], "end" => ["x" => 1]],
        ];

        foreach ($inputs as $input) {
            $this->json("POST", "/api/territories", $input)
                ->assertStatus(400)
                ->assertJson([
                    "data" => $input,
                    "error" => "territories/incomplete-data"
                ]);
        }

    }

    public function testShouldListTerritories()
    {
        $territory1 = [
            'name' => 'A',
            'start' => ['x' => 0, 'y' => 0],
            'end' => ['x' => 0, 'y' => 0],
        ];
        $this->insertTerritory($territory1);

        $territory2 = [
            'name' => 'B',
            'start' => ['x' => 9, 'y' => 9],
            'end' => ['x' => 9, 'y' => 9],
        ];
        $this->insertTerritory($territory2);

        $this->json("GET", "/api/territories")
            ->assertStatus(200)
            ->assertJson([
                "count" => 2,
                "data" => [$territory1, $territory2],
            ]);

    }

    public function testShouldDeleteTerritory()
    {
        $territory1 = [
            'name' => 'A',
            'start' => ['x' => 0, 'y' => 0],
            'end' => ['x' => 0, 'y' => 0],
        ];
        $territoryInstance1 = $this->insertTerritory($territory1);

        $territory2 = [
            'name' => 'B',
            'start' => ['x' => 9, 'y' => 9],
            'end' => ['x' => 9, 'y' => 9],
        ];
        $this->insertTerritory($territory2);

        $this->json("DELETE", "/api/territories/{$territoryInstance1->_id}")
            ->assertStatus(200)
            ->assertJson([
                "error" => false
            ]);

        $this->json("GET", "/api/territories")
            ->assertStatus(200)
            ->assertJson([
                "count" => 1,
                "data" => [$territory2],
            ]);

        $this->json("DELETE", "/api/territories/12300")
            ->assertStatus(404)
            ->assertJson(['error' => "territories/not-found"]);
    }

    public function testShouldGetOneTerritory()
    {
        $territory = [
            'name' => 'A',
            'start' => ['x' => 0, 'y' => 0],
            'end' => ['x' => 0, 'y' => 0],
        ];
        $territoryInstance = $this->insertTerritory($territory);

        $this->json("GET", "/api/territories/{$territoryInstance->_id}")
            ->assertStatus(200)
            ->assertJson([
                "data" => $territory,
                "error" => false
            ]);

        $this->json("GET", "/api/territories/12300")
            ->assertStatus(404)
            ->assertJson(['error' => "territories/not-found"]);
    }
}
