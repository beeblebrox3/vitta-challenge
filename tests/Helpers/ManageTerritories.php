<?php

namespace Tests\Helpers;

use App\Territory;

trait ManageTerritories
{
    /**
     * @param array $data
     * @return Territory
     */
    protected function insertTerritory(array $data)
    {
        $obj = (new Territory())->fillFromTwoPoints(
            $data['name'],
            $data['start']['x'],
            $data['start']['y'],
            $data['end']['x'],
            $data['end']['y']
        );
        $obj->save();
        return $obj;
    }
}