<?php

namespace Tests\Unit;

use App\DTO\Location;
use Tests\TestCase;
use App\Helpers\TwoPointsArea;

class TwoPointsAreaTest extends TestCase
{
    protected function getLocation(int $startX, int $startY, int $endX, int $endY)
    {
        return TwoPointsArea::getLocation($startX, $startY, $endX, $endY);
    }

    public function testDetectPoint()
    {
        $location = $this->getLocation(8, 4, 8, 4);
        $this->assertEquals(Location::TYPE_POINT, $location->get('type'));
    }

    public function testDetectHorizontalLine()
    {
        $location = $this->getLocation(7, 7, 10, 7);
        $this->assertEquals(Location::TYPE_LINE, $location->get('type'));

        $this->assertEquals(7, $location->get('coordinates')[0][0]);
        $this->assertEquals(7, $location->get('coordinates')[0][1]);
        $this->assertEquals(10, $location->get('coordinates')[1][0]);
        $this->assertEquals(7, $location->get('coordinates')[1][1]);
    }

    public function testDetectVerticalLine()
    {
        $location = $this->getLocation(4, 1, 4, 3);
        $this->assertEquals(Location::TYPE_LINE, $location->get('type'));

        $this->assertEquals(4, $location->get('coordinates')[0][0]);
        $this->assertEquals(1, $location->get('coordinates')[0][1]);
        $this->assertEquals(4, $location->get('coordinates')[1][0]);
        $this->assertEquals(3, $location->get('coordinates')[1][1]);
    }

    public function testDetectPolygon()
    {
        $location = $this->getLocation(2, 5, 6, 9);
        $this->assertEquals(Location::TYPE_POLYGON, $location->get('type'));

        $this->assertEquals(2, $location->get('coordinates')[0][0][0]);
        $this->assertEquals(5, $location->get('coordinates')[0][0][1]);
        $this->assertEquals(2, $location->get('coordinates')[0][1][0]);
        $this->assertEquals(9, $location->get('coordinates')[0][1][1]);
        $this->assertEquals(6, $location->get('coordinates')[0][2][0]);
        $this->assertEquals(9, $location->get('coordinates')[0][2][1]);
        $this->assertEquals(6, $location->get('coordinates')[0][3][0]);
        $this->assertEquals(5, $location->get('coordinates')[0][3][1]);
        $this->assertEquals(2, $location->get('coordinates')[0][4][0]);
        $this->assertEquals(5, $location->get('coordinates')[0][4][1]);
    }

    public function testDetectArea() {
        $data = [
            [1, 8, 4, 8, 4],
            [4, 7, 7, 10, 7],
            [3, 4, 1, 4, 3],
            [25, 2, 5, 6, 9]
        ];

        foreach ($data as $info) {
            $this->assertEquals($info[0], TwoPointsArea::calculateArea($info[1], $info[2], $info[3], $info[4]));
        }
    }
}
