<?php

namespace App;

use App\DTO\Location;
use App\Exceptions\TerritoryOverlapping;
use App\Helpers\TwoPointsArea;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Jenssegers\Mongodb\Eloquent\Model;

class Territory extends Model
{
    protected $collection = "territories";

    protected $fillable = [
        'name',
        'start',
        'end',
        'location',
        'area',
    ];

    protected $validationRules = [
        'name' => 'required|string|min:1|unique:territories,name',
        'start.x' => 'required|integer|min:0|max:99',
        'start.y' => 'required|integer|min:0|max:99',
        'end.x' => 'required|integer|min:0|max:99',
        'end.y' => 'required|integer|min:0|max:99',
        'location.type' => 'required|string|in:Polygon,LineString,Point',
        'location.coordinates' => 'required|array',
    ];

    protected $appends = [];

    /**
     * @return HasMany
     */
    public function squares()
    {
        return $this->hasMany(Square::class, "territory_id", "_id");
    }

    /**
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function paintedSquares()
    {
        $this->setAppends(array_merge($this->appends, ['painted_area']));
        return $this->hasMany(Square::class, "territory_id", "_id")
            ->whereNotNull("color");
    }

    /**
     * @return int
     */
    public function getPaintedAreaAttribute()
    {
        return $this->paintedSquares->count();
    }

    /**
     * @return float|int
     */
    public function getProportionalPaintedAreaAttribute()
    {
        return $this->painted_area / max($this->area, 1);
    }

    public function enableStatistics()
    {
        $this->setAppends(
            array_merge($this->appends, ['proportional_painted_area'])
        );
        return $this;
    }

    /**
     * @param string $name
     * @param int $startX
     * @param int $startY
     * @param int $endX
     * @param int $endY
     * @return self
     */
    public function fillFromTwoPoints(string $name, int $startX, int $startY, int $endX, int $endY)
    {
        $location = TwoPointsArea::getLocation($startX, $startY, $endX, $endY);

        $this->fill([
            'name' => $name,
            'start' => ['x' => $startX, 'y' => $startY],
            'end' => ['x' => $endX, 'y' => $endY],
            'area' => TwoPointsArea::calculateArea($startX, $startY, $endX, $endY),
            'location' => $location->toArray()
        ]);

        return $this;
    }

    /**
     * @param array $options
     * @return bool|void
     */
    public function save(array $options = [])
    {
        $location = TwoPointsArea::getLocation($this->start['x'], $this->start['y'], $this->end['x'], $this->end['y']);
        $this->isOverlapping($location);
        parent::save($options);
    }

    /**
     * @param Location $location
     * @throws TerritoryOverlapping
     */
    public function isOverlapping(Location $location)
    {
        $overlaping = $this->where('location', 'geoIntersects', [
            '$geometry' => $location->toArray()
        ])->whereNotIn('_id', [$this->_id])->count();

        if ($overlaping) {
            throw new TerritoryOverlapping();
        }
    }

    /**
     * @param Location $location
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getByLocation(Location $location)
    {
        return $this->where('location', 'geoIntersects', [
            '$geometry' => $location->toArray()
        ])->first();
    }

    /**
     * @param string $color
     */
    public function paint(string $color)
    {
        foreach (range($this->getAttribute("start.x"), $this->getAttribute("end.x")) as $x) {
            foreach (range($this->getAttribute("start.y"), $this->getAttribute("end.y")) as $y) {
                $this->paintedSquares()->updateOrInsert([
                    'x' => $x,
                    'y' => $y,
                ], ['color' => $color, 'territory_id' => $this->_id]);
            }
        }
    }
}
