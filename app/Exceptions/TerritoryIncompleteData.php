<?php

namespace App\Exceptions;


use Illuminate\Contracts\Support\MessageBag;

class TerritoryIncompleteData extends \Exception
{
    /** @var string */
    protected $message = "territories/incomplete-data";

    /** @var MessageBag|null */
    protected $errors = null;

    /**
     * @param MessageBag $errors
     */
    public function setErros(MessageBag $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return MessageBag|null
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
