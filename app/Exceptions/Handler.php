<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\ExceptionLog;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [];

    /**
     *
     * @param Exception $exception
     */
    public function report(Exception $exception)
    {
        ExceptionLog::createFromException($exception);
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $statusCode = 500;

        if ($exception instanceof TerritoryIncompleteData || $exception instanceof TerritoryOverlapping) {
            $statusCode = 400;
        }

        if ($exception instanceof TerritoryNotFound || $exception instanceof SquareNotFound) {
            $statusCode = 404;
        }

        return response()->json([
            "data" => $request->all(),
            "error" => $exception->getMessage(),
        ])->setStatusCode($statusCode);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Illuminate\Auth\AuthenticationException $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }
}
