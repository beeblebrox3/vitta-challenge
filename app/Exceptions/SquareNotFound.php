<?php

namespace App\Exceptions;

class SquareNotFound extends \Exception
{
    /** @var string */
    protected $message = "squares/not-found";

}
