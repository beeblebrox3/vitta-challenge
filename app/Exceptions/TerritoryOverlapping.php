<?php

namespace App\Exceptions;

class TerritoryOverlapping extends \Exception
{
    /** @var string */
    protected $message = "territories/territory-overlay";

}
