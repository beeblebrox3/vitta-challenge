<?php

namespace App\Exceptions;

class TerritoryNotFound extends \Exception
{
    /** @var string */
    protected $message = "territories/not-found";

}
