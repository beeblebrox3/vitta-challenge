<?php

namespace App;

use App\Exceptions\TerritoryIncompleteData;

class ExceptionLog extends Model
{
    protected $collection = "logs";

    protected $fillable = [
        'type',
        'message',
        'line',
        'file',
        'trace',
        'errors',
    ];

    /**
     * @param \Exception $exception
     * @return static
     */
    public static function createFromException(\Exception $exception)
    {
        $obj = new static;

        $logData = [
            'type' => get_class($exception),
            'message' => $exception->getMessage(),
            'line' => $exception->getLine(),
            'file' => $exception->getFile(),
            'trace' => $exception->getTraceAsString(),
        ];

        if ($exception instanceof TerritoryIncompleteData) {
            $logData['errors'] = $exception->getErrors()->toArray();
        }

        $obj->fill($logData);
        $obj->save();

        return $obj;
    }
}
