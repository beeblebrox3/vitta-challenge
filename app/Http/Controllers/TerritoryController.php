<?php

namespace App\Http\Controllers;

use App\Exceptions\TerritoryIncompleteData;
use App\Exceptions\TerritoryNotFound;
use App\Territory;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class TerritoryController extends Controller
{
    private $model;

    public function __construct(Territory $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->apiformat($this->model->all(), ['count' => $this->model->count()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);
        $obj = $this->model->newInstance();
        $obj->fillFromTwoPoints(
            $request->input('name'),
            $request->input('start.x'),
            $request->input('start.y'),
            $request->input('end.x'),
            $request->input('end.y')
        )
        ->save();

        return response()->apiformat($obj);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $territory = $this->findTerritory($id);
        if ($request->input('withpainted') === "true") {
            $territory->load("paintedSquares");
        }
        return response()->apiformat($territory);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws TerritoryNotFound
     */
    public function destroy($id)
    {
        $obj = $this->findTerritory($id);

        $obj->delete();
        return response()->json(['error' => false]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function paint(Request $request, $id)
    {
        $this->validate($request, ['color' => 'string|required']);

        $obj = $this->findTerritory($id);

        $obj->paint($request->input("color"));
        $obj->save();

        $obj->refresh();
        $obj->load('paintedSquares');

        return response()->apiformat($obj);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function removePaint($id)
    {
        $obj = $this->findTerritory($id);
        $obj->paintedSquares()->delete();

        $obj->refresh();
        return response()->apiformat($obj);
    }

    private function validateRequest(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required|string|min:1',
                'start.x' => 'required|integer|min:0|max:99',
                'start.y' => 'required|integer|min:0|max:99',
                'end.x' => 'required|integer|min:0|max:99',
                'end.y' => 'required|integer|min:0|max:99',
            ]);
        } catch (ValidationException $exception) {
            $newException = new TerritoryIncompleteData();
            $newException->setErros($exception->validator->getMessageBag());

            throw $newException;
        }
    }

    /**
     * @param $id
     * @return Territory
     * @throws TerritoryNotFound
     */
    private function findTerritory($id)
    {
        $obj = $this->model->find($id);
        if (!$obj) {
            throw new TerritoryNotFound();
        }

        return $obj;
    }
}
