<?php

namespace App\Http\Controllers;

use App\Exceptions\SquareNotFound;
use App\Helpers\TwoPointsArea;
use App\Square;
use App\Territory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SquareController extends Controller
{
    private $model;

    public function __construct(Square $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $x
     * @param int $y
     * @return Response
     */
    public function show(int $x, int $y)
    {
        return response()->apiformat($this->getSquare((int)$x, (int)$y));
    }

    /**
     * @param Request $request
     * @param int $x
     * @param int $y
     * @return Response
     */
    public function update(Request $request, int $x, int $y)
    {
        $square = $this->getSquare($x, $y);
        $square->color = $request->input('color');
        $square->save();

        return response()->apiformat($square);
    }

    /**
     * @param int $x
     * @param int $y
     * @return mixed|static
     * @throws SquareNotFound
     */
    private function getSquare(int $x, int $y)
    {
        $territory = new Territory();
        $territory = $territory->getByLocation(TwoPointsArea::getLocationFromPoint($x, $y));

        if (!$territory) {
            throw new SquareNotFound();
        }

        $territory->load(['squares' => function (HasMany $queryBuilder) use ($x, $y, $territory) {
            $queryBuilder->where(['x' => $x, 'y' => $y, 'territory_id' => $territory->_id]);
        }]);

        if ($territory->squares->count()) {
            return $territory->squares->first();
        }

        return (new Square([
            'x' => $x,
            'y' => $y,
            'territory_id' => $territory->_id,
        ]));
    }
}
