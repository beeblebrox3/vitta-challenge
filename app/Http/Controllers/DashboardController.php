<?php

namespace App\Http\Controllers;

use App\ExceptionLog;
use App\Helpers\HtmlMapDrawer;
use App\Square;
use App\Territory;

class DashboardController extends Controller
{
    protected $territoryModel;
    protected $squareModel;
    protected $logModel;

    public function __construct(Territory $territoryModel, Square $squareModel, ExceptionLog $logModel)
    {
        $this->territoryModel = $territoryModel;
        $this->squareModel = $squareModel;
        $this->logModel = $logModel;
    }

    public function index()
    {
        $territories = $this->territoryModel->enableStatistics()->with('paintedSquares', 'squares')->get();
        $squares = $this->squareModel->whereNotNull('color')->orderBy('updated_at', 'desc')->take(5)->get();
        $totalPaintedArea = 0;

        $htmlBuilder = new HtmlMapDrawer(99, 99);
        foreach ($territories as $territory) {
            $htmlBuilder->paintArea(
                $territory->start['x'],
                $territory->start['y'],
                $territory->end['x'],
                $territory->end['y'],
                'silver'
            );
            foreach ($territory->paintedSquares as $square) {
                $htmlBuilder->paintPoint($square->x, $square->y, $square->color ?: "silver");
                $totalPaintedArea++;
            }
        }

        $logs = $this->logModel->orderBy('created_at', 'desc')->take(5)->get();

        return view("dashboard")
            ->with('territories', $territories)
            ->with('squares', $squares)
            ->with('chart', $htmlBuilder->draw())
            ->with('logs', $logs)
            ->with('totalPaintedArea', $totalPaintedArea)
            ->with('totalArea', 99 * 99);
    }
}