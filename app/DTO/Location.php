<?php

namespace App\DTO;

class Location extends AbstractDTO
{
    const TYPE_LINE = 'LineString';
    const TYPE_POLYGON = 'Polygon';
    const TYPE_POINT = 'Point';

    protected $validationRules = [
        'type' => 'required|string|in:Polygon,LineString,Point',
        'coordinates' => 'required|array',
    ];
}
