<?php

namespace App\DTO;

use Illuminate\Support\MessageBag;
use Illuminate\Validation\Validator;

abstract class AbstractDTO
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var array
     */
    protected $dirty = [];

    /**
     * @var array
     */
    protected $validationRules = [];

    /**
     * @var Validator
     */
    protected $validator;

    /**
     * Validation errors
     * @var MessageBag|null
     */
    protected $errors;

    /**
     * DTO constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $this->filter($data);
    }

    /**
     * Update the object data
     *
     * @param array $data the object data
     * @param bool $replace if true, will replace the current data. If false, will merge it
     */
    public function fill(array $data = [], $replace = false)
    {
        if ($replace === true) {
            $this->data = [];
            $this->dirty = [];
        }

        $data = $this->filter($data);

        foreach ($data as $key => $value) {
            $this->__set($key, $value);
        }
    }

    /**
     * Filter the data
     *
     * @param array $data
     *
     * @return array
     */
    public function filter(array $data = [])
    {
        return $data;
    }

    /**
     * Set the property value
     *
     * @param string $name the property name
     * @param mixed $value the value
     */
    public function __set($name, $value)
    {
        $this->dirty[$name] = true;
        $this->data[$name] = $value;
    }

    /**
     * Returns the property value
     *
     * @param string $name the property
     *
     * @return mixed the property value
     */
    public function __get($name)
    {
        return $this->data[$name];
    }

    /**
     * @param string $name
     * @param mixed $defaultValue
     * @return mixed
     */
    public function get($name, $defaultValue = null)
    {
        return $this->has($name) ? $this->data[$name] : $defaultValue;
    }

    /**
     * @param string $name
     * @param $value
     */
    public function set($name, $value)
    {
        $this->__set($name, $value);
    }

    /**
     * Retorna os valores das chaves especificadas
     *
     * @param array $keys
     * @param mixed $defaultValue
     * @return array
     */
    public function only(array $keys, $defaultValue = null)
    {
        $dados = $this->toArray();
        $response = array_fill_keys($keys, $defaultValue);

        $dados = array_merge($response, $dados);
        return array_only($dados, $keys);
    }

    /**
     * Retorna os valores, exceto as chaves informadas
     *
     * @param array $keys
     * @return array
     */
    public function except(array $keys)
    {
        return array_except($this->toArray(), $keys);
    }

    /**
     * Verifica se a informação inserida existe
     *
     * @param string $name
     * @return bool
     */
    public function has($name)
    {
        return array_key_exists($name, $this->data);
    }

    /**
     * Returns the changed data
     *
     * @return array the changed data
     */
    public function getDirty()
    {
        return $this->dirty;
    }

    /**
     * Checks if the object property changed
     *
     * @param $name
     *
     * @return bool
     */
    public function isDirty($name)
    {
        if (!$this->has($name)) {
            throw new \InvalidArgumentException("$name doesn't exists.");
        }

        return isset($this->dirty[$name]);
    }

    /**
     * Converts object data to array
     *
     * @return array the object data in array format
     */
    public function toArray()
    {
        return $this->data;
    }

    /**
     * Return all DTO data
     * @see static::toArray()
     * @return array
     */
    public function all()
    {
        return $this->toArray();
    }

    /**
     * Converts object data to JSON
     *
     * @return string the object data in JSON format
     */
    public function toJson()
    {
        return json_encode($this->data);
    }

    /**
     * Checks if the data is valid
     *
     * @return bool true if data is valid or false otherwise
     */
    public function isValid()
    {
        $this->validator = $this->validator ? $this->validator : \Validator::make($this->data, $this->validationRules);

        if ($this->getDirty()) {
            $this->validator->setData($this->data);
        }

        $isValid = $this->validator->passes();
        if (!$isValid) {
            $this->errors = $this->validator->errors();
        }

        return $isValid;
    }

    /**
     * Returns validation errors
     *
     * @return MessageBag|null
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return MessageBag
     */
    protected function getOrCreateErrors()
    {
        if (!$this->errors) {
            $this->errors = new MessageBag;
        }

        return $this->errors;
    }
}
