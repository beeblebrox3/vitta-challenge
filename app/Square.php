<?php

namespace App;

class Square extends Model
{
    protected $collection = "squares";

    protected $fillable = [
        'x',
        'y',
        'color',
        'territory_id'
    ];

    protected $validationRules = [
        'territory_id' => 'required',
        'x' => 'required|integer|min:0|max:99',
        'y' => 'required|integer|min:0|max:99',
        'color' => 'string|max:24|nullable'
    ];

    protected $appends = [
        'painted',
    ];

    public function territory()
    {
        return $this->belongsTo("Territory", "territories_id", "_id");
    }

    public function getPaintedAttribute()
    {
        if ($this->getAttribute('color')) {
            return true;
        }

        return false;
    }
}