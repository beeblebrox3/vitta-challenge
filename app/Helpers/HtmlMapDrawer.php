<?php

namespace App\Helpers;

class HtmlMapDrawer extends MapDrawer
{
    /** @var array  */
    protected $paintedArea = [];

    /** @var string  */
    public $tableStyle = "border: 1px solid black; border-collapse: collapse;";

    /** @var string  */
    public $trStyle = "";

    /** @var string  */
    public $tdStyle = "border: 1px solid silver; width: 10px; height: 10px; ";

    /** @var string  */
    public $thStyle = "background: silver;";

    /** @var string  */
    protected $markup = "";

    /**
     * @return string
     */
    public function draw()
    {
        $this->tag("table", false, $this->tableStyle);

        $this->printXAxis();

        foreach (array_reverse(range(0, $this->maxY)) as $y) {
            $this->tag("tr", false, $this->trStyle);

            $firstColumn = true;
            foreach (range(0, $this->maxX) as $x) {
                if ($firstColumn) {
                    $this->tag("th", false, $this->thStyle);
                    $this->mkp($y);
                    $this->tag("th", true);
                    $firstColumn = false;
                }

                $this->tag("td", false, $this->tdStyle . $this->getPaintedStyle($x, $y));
                $this->tag("td", true);
            }
            $this->tag("tr", true);
        }

        $this->printXAxis();

        $this->tag("table", true);
        return $this->markup;
    }

    /**
     * @param int $startX
     * @param int $startY
     * @param int $endX
     * @param int $endY
     * @param string $color
     * @return $this
     */
    public function paintArea(int $startX, int $startY, int $endX, int $endY, string $color)
    {
        foreach (range($startX, $endX) as $x) {
            foreach (range($startY, $endY) as $y) {
                $this->paintPoint($x, $y, $color);
            }
        }

        return $this;
    }

    /**
     * @param int $x
     * @param int $y
     * @param string $color
     * @return $this
     */
    public function paintPoint(int $x, int $y, string $color)
    {
        if ($x > $this->maxX) {
            throw new \OutOfBoundsException("The X coordinate cannot be higher than {$this->maxX}");
        }

        if ($y > $this->maxY) {
            throw new \OutOfBoundsException("The X coordinate cannot be higher than {$this->maxY}");
        }

        $this->paintedArea[$x][$y] = "background-color: $color";

        return $this;
    }

    /**
     * @param int $x
     * @param int $y
     * @return string
     */
    protected function getPaintedStyle(int $x, int $y)
    {
        if (!empty($this->paintedArea[$x][$y])) {
            return $this->paintedArea[$x][$y];
        }

        return "";
    }

    /**
     * @param $tagName
     * @param bool $close
     * @param string $style
     * @return HtmlMapDrawer
     */
    private function tag($tagName, $close = false, $style = "")
    {
        if ($close) {
            return $this->mkp("</$tagName>");
        }

        return $this->mkp("<$tagName style='$style'>");
    }

    /**
     * @param $content
     * @return $this
     */
    private function mkp($content)
    {
        $this->markup .= $content;
        return $this;
    }

    private function printXAxis()
    {
        $this->tag("tr", false, $this->trStyle);
        $this->tag("th", false, $this->thStyle);
        $this->tag("th", true);
        foreach (range(0, $this->maxX) as $x) {
            $this->tag("th", false, $this->thStyle);
            $this->mkp($x);
            $this->tag("th", true);
        }
        $this->tag("tr", true);
    }
}
