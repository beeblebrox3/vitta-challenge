<?php

namespace App\Helpers;

abstract class MapDrawer
{
    /** @var int */
    protected $maxX;

    /** @var int */
    protected $maxY;

    public function __construct(int $maxX, int $maxY)
    {
        $this->maxX = $maxX;
        $this->maxY = $maxY;
    }

    /**
     * @return mixed
     */
    abstract public function draw();

    /**
     * @param int $startX
     * @param int $startY
     * @param int $endX
     * @param int $endY
     * @param string $color
     * @return $this
     */
    abstract public function paintArea(int $startX, int $startY, int $endX, int $endY, string $color);

    /**
     * @param int $x
     * @param int $y
     * @param string $color
     * @return $this
     */
    abstract public function paintPoint(int $x, int $y, string $color);
}