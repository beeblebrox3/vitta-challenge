<?php

namespace App\Helpers;

use App\DTO\Location;

class TwoPointsArea
{
    /**
     * @param int $startX
     * @param int $startY
     * @param int $endX
     * @param int $endY
     * @return bool
     */
    protected static function isLine(int $startX, int $startY, int $endX, int $endY)
    {
        return $startX === $endX || $startY === $endY;
    }

    /**
     * @param int $startX
     * @param int $startY
     * @param int $endX
     * @param int $endY
     * @return bool
     */
    protected static function isPoint(int $startX, int $startY, int $endX, int $endY)
    {
        return $startX === $endX && $startY === $endY;
    }

    /**
     * @param int $startX
     * @param int $startY
     * @param int $endX
     * @param int $endY
     * @return Location
     */
    protected static function makeLine(int $startX, int $startY, int $endX, int $endY)
    {
        return new Location([
            'type' => Location::TYPE_LINE,
            'coordinates' => [
                [$startX, $startY],
                [$endX, $endY]
            ]
        ]);
    }

    /**
     * @param int $x
     * @param int $y
     * @return Location
     */
    protected static function makePoint(int $x, int $y)
    {
        return new Location([
            'type' => Location::TYPE_POINT,
            'coordinates' => [$x, $y]
        ]);
    }

    /**
     * @param int $startX
     * @param int $startY
     * @param int $endX
     * @param int $endY
     * @return Location
     */
    protected static function makePolygon(int $startX, int $startY, int $endX, int $endY)
    {
        return new Location([
            'type' => Location::TYPE_POLYGON,
            'coordinates' => [[
                [$startX, $startY],
                [$startX, $endY],
                [$endX, $endY],
                [$endX, $startY],
                [$startX, $startY]
            ]]
        ]);
    }

    /**
     * @param int $startX
     * @param int $startY
     * @param int $endX
     * @param int $endY
     * @return Location
     */
    public static function getLocation(int $startX, int $startY, int $endX, int $endY)
    {
        if (static::isPoint($startX, $startY, $endX, $endY)) {
            return static::makePoint($startX, $startY);
        }

        if (static::isLine($startX, $startY, $endX, $endY)) {
            return static::makeLine($startX, $startY, $endX, $endY);
        }

        return static::makePolygon($startX, $startY, $endX, $endY);
    }

    /**
     * @param int $x
     * @param int $y
     * @return Location
     */
    public static function getLocationFromPoint(int $x, int $y)
    {
        return static::makePoint($x, $y);
    }

    /**
     * @param int $startX
     * @param int $startY
     * @param int $endX
     * @param int $endY
     * @return int
     */
    public static function calculateArea(int $startX, int $startY, int $endX, int $endY)
    {
        return ($endX - $startX + 1) * ($endY - $startY + 1);
    }
}
