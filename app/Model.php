<?php

namespace App;

use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Validation\ValidationException;
use Validator;

abstract class Model extends \Jenssegers\Mongodb\Eloquent\Model
{
    /** @var MessageBag|null */
    protected $validationErrors = null;

    /** @var array */
    protected $validationRules = [];

    /**
     * @return void
     * @throws ValidationException
     */
    public function validate()
    {
        if (!$this->validationRules) {
            return;
        }

        $data = $this->getAttributes();

        $validator = Validator::make($data, $this->validationRules);
        if ($validator->fails()) {
            $this->validationErrors = $validator->errors();
            throw new ValidationException($validator);
        }
    }

    public function save(array $options = [])
    {
        $this->validate();
        return parent::save($options);
    }
}
